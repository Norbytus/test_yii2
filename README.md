Test Task with country

First set database and did migration 

Command List

- city                         Class: CityController
    city/add                   Add city in country

- country                      Class: CountryController
    country/add                Add country
    country/delete             Delete Country
    country/list               Show Country list
    country/list-with-cities   Show country list with cities

