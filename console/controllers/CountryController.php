<?php

namespace console\controllers;

use yii\console\Controller;
use app\models\Country;

/**
 * Class: CountryController
 *
 * @see Controller
 */
class CountryController extends Controller
{
    /**
     * Show Country list
     *
     */
    public function actionList()
    {
        foreach (Country::find()->all() as $country) {
            echo $country->name . "\n";
        }
    }

    /**
     * Show country list with cities
     *
     */
    public function actionListWithCities()
    {
        foreach (Country::find()->with('cities')->all() as $country) {
            $pattern = "Country %s \n";
            echo sprintf($pattern, $country->name);
            if (!empty($country->cities)) {
                foreach ($country->cities as $city) {
                    $pattern = "---- City %s \n";
                    echo sprintf($pattern, $city->name);
                }
            }
        }
    }

    /**
     * Add country
     *
     * @param string $name
     */
    public function actionAdd(string $name)
    {
        $country = new Country();
        $country->name = $name;

        if (!$country->save()) {
            foreach ($country->getErrors() as $error) {
                echo $error[0] . "\n";
            }
        } else {
            $pattern = 'Country %s success added';
            sprintf($pattern, $country->name);
        }
    }

    /**
     * Delete Country
     *
     * @param string $name
     */
    public function actionDelete(string $name)
    {
        $country = Country::find(['name' => $name])->one();
        if ($country && $country->delete()) {
            $pattern = "Country %s deleted\n";
            echo sprintf($pattern, $name);
        } else {
            $pattern = "Country with %s not exists\n";
            echo sprintf($pattern, $name);
        }
    }
}
