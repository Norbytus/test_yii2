<?php

namespace console\controllers;

use yii\console\Controller;
use app\models\{Country, City};

/**
 * Class: CityController
 *
 * @see Controller
 */
class CityController extends Controller
{
    /**
     * Add city in country
     *
     * @param string $cityName
     * @param string $countryName
     */
    public function actionAdd(string $cityName, string $countryName)
    {
        $country = Country::find(['name' => $countryName])->one();

        if (!$country) {
            $pattern = "Country %s not exists\n";
            sprintf($pattern, $countryName);
        }

        $city = new City;
        $city->name = $cityName;
        $city->country_id = $country->id;
        if (!$city->save()) {
            foreach ($city->getErrors() as $error) {
                echo $error[0] . "\n";
            }
        } else {
            $pattern = "City %s success added for country %s\n";
            echo sprintf($pattern, $city->name, $country->name);
        }

    }
}
