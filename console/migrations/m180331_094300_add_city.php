<?php

use yii\db\Migration;

/**
 * Class m180331_094300_add_city
 */
class m180331_094300_add_city extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{country}}', ['name' => 'Russia']);
        $this->insert('{{country}}', ['name' => 'USA']);
        $this->insert('{{country}}', ['name' => 'Britania']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180331_094300_add_city cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180331_094300_add_city cannot be reverted.\n";

        return false;
    }
    */
}
