<?php

use yii\db\Migration;

/**
 * Class m180331_082518_add_tables
 */
class m180331_082518_add_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            '{{country}}',
            [
                'id' => 'pk',
                'name' => 'string NOT NULL',
            ],
            'ENGINE=InnoDB CHARSET=utf8'
        );
        $this->createTable(
            '{{city}}',
            [
                'id' => 'pk',
                'name' => 'string NOT NULL',
                'country_id' => 'int NOT NULL',
            ],
            'ENGINE=InnoDB CHARSET=utf8'
        );

        $this->createIndex('country_index', '{{city}}', 'country_id');

        $this->addForeignKey(
            'city_to_country',
            '{{city}}',
            'country_id',
            '{{country}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180331_082518_add_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180331_082518_add_tables cannot be reverted.\n";

        return false;
    }
    */
}
