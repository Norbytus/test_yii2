<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class: Country
 *
 * @see ActiveRecord
 */
class Country extends ActiveRecord
{

    /**
     * name
     *
     * @var string
     */
    protected $name;

    public static function tableName(): string
    {
        return '{{country}}';
    }

    public function rules(): array
    {
        return [
            ['name', 'string'],
            ['name', 'required'],
            ['name', 'unique'],
        ];
    }

    public function getCities()
    {
        return $this->hasMany(City::className(), ['country_id' => 'id']);
    }

}
