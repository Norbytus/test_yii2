<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class: City
 *
 * @see ActiveRecord
 */
class City extends ActiveRecord
{

    public static function tableName(): string
    {
        return '{{city}}';
    }

    public function rules(): array
    {
        return [
            ['name', 'string'],
            ['name', 'required'],
            [['name'], 'unique'],
        ];
    }

}
